$(document).ready(function(){
    var reference;
    $(".icon-delete").click(function() {
        reference = this;
    });
    $(".modal-delete-btn").click(function() {
        $("#deleteModal").modal('hide');
        $(reference).next().submit();
    });
    $(".article-btns").click(function() {
       $(this).parent().submit();
    });
});