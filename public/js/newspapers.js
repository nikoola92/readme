$(document).ready(function(){
    $(".newspapers-create-form").validate({
        rules: {
            name: "required",
            owner: "required",
            image: "required"
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents("div.form-group").addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents("div.form-group").removeClass('has-error');
        },
        errorPlacement: function(error, element) {
            if($(element).attr("type") === 'file')
                $(element).closest(".form-group").append(error);
            else
                $(element).parent().append(error);
        }
    });
});
