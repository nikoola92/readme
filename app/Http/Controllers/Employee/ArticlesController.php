<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\NewspaperArticle;
use App\Models\Newspaper;
use App\Models\NewspaperEmployee;
use Illuminate\Http\Request;
use App\Http\Requests;

class ArticlesController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $articles = NewspaperArticle::where('user_id', $user->id)
                                    ->where('approved', 1)
                                    ->paginate(10);

        return view('employee.articles.index', ['articles' => $articles, 'activeMenu' => 'articles']);
    }

    public function create()
    {
        return view('employee.articles.create');
    }

    public function show($id)
    {
        $article = NewspaperArticle::find($id);
        return view('employee.articles.show', ['article' => $article, 'activeMenu' => 'articles']);
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $employee = NewspaperEmployee::where('user_id', $user->id)->first();
        $newspaper = Newspaper::where('id', $employee->newspaper_id)->first();
        $article =  new NewspaperArticle();
        $article->title = $request->input('title');
        $article->content = $request->input('content');
        $article->approved = 0;
        $file = $request->file('image');

        $extension = $file->getClientOriginalExtension();
        $filename = microtime()*10000 . $article->name . '.' . $extension;
        $file->move('images/articles', $filename);
        $article->img_url = $filename;

        $article->user_id = $user->id;
        $article->newspaper_id = $newspaper->id;
        $article->save();
        return redirect()->route('employee.articles.index');
    }

    public function edit($id)
    {
        $article = NewspaperArticle::find($id);
        return view('employee.articles.edit', ['article' => $article, 'activeMenu' => 'articles']);
    }

    public function update(Request $request, $id)
    {
        $article = NewspaperArticle::find($id);
        $article->title = $request->input('title');
        $article->content = $request->input('content');
        //TODO update picture
        $article->update();
        return redirect()->route('employee.articles.index');
    }
}
