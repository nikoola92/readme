<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\NewspaperArticle;

class UserController extends Controller
{
    public function index()
    {
        $articles = NewspaperArticle::paginate(10);
        return view('home', ['articles' => $articles]);
    }

}
