<?php

namespace App\Http\Controllers\Owner;


use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\NewspaperEmployee;
use App\Models\Newspaper;
use App\Models\User;
use App\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    //
    public function index()
    {
        $owner = Auth::user();

        $newspaper = Newspaper::where('user_id', $owner->id)->first();
        $employees = NewspaperEmployee::where('newspaper_id', $newspaper->id)->get();
        $ids = [];
        foreach($employees as $employee)
        {
            array_push($ids, $employee->user_id);
        }
        $users = User::whereIn('id', $ids)->get();
        return view('owner.dashboard', ['users' => $users, 'newspaper' => $newspaper, 'activeMenu' => 'dashboard']);
    }

    public function editInfo()
    {
        $owner = Auth::user();

        $newspaper = Newspaper::where('user_id', $owner->id)->first();

        return view('owner.edit-info', ['newspaper' => $newspaper, 'activeMenu' => 'dashboard']);
    }

    public function hire()
    {
        $users = User::where('hired', 0)->get();
        return view('owner.hire', ['users' => $users, 'activeMenu' => 'dashboard']);
    }

    public function hireNew(Request $request)
    {
        $owner = Auth::user();
        $newspaper = Newspaper::where('user_id', $owner->id)->first();
        $employee = new NewspaperEmployee();
        $employee->user_id = $request->input('user_id');
        $employee->newspaper_id = $newspaper->id;
        $employee->save();
        DB::table('role_user')->where('user_id', $request->input('user_id'))->update(['role_id' => 3]);
        return redirect()->route('owner.dashboard');
    }

    public function fireUser(Request $request)
    {
        NewspaperEmployee::where('user_id', $request->input('id'))->delete();
        $user = User::find($request->input('id'));
        $user->hired = 0;
        $user->save();
        DB::table('role_user')->where('user_id', $request->input('id'))->update(['role_id' => 4]);
        return redirect()->route('owner.dashboard');
    }

    public function editInfoPost(Request $request)
    {
        $newspaper = Newspaper::find($request->input('id'));
        $newspaper->newspapers_name = $request->input('name');
        $newspaper->save();
        //TODO CHANGE PICTURE
        return redirect()->route('owner.dashboard');
    }

}
