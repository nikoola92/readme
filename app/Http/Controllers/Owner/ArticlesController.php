<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use App\Models\NewspaperArticle;
use App\Models\Newspaper;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class ArticlesController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $newspaper = Newspaper::where('user_id', $user->id)->first();
        $articles = NewspaperArticle::join('users', 'users.id', '=', 'newspaper_articles.user_id')
                                    ->selectRaw('newspaper_articles.*, users.username as username')
                                    ->where('newspaper_id', $newspaper->id)
                                    ->where('approved', 1)
                                    ->paginate(10);
        $pendingArticles = NewspaperArticle::join('users', 'users.id', '=', 'newspaper_articles.user_id')
            ->selectRaw('newspaper_articles.*, users.username as username')
            ->where('newspaper_id', $newspaper->id)
            ->where('approved', 0)
            ->paginate(10);

        return view('owner.articles.index', ['articles' => $articles, 'pendingArticles' => $pendingArticles, 'activeMenu' => 'articles']);
    }

    public function create()
    {
        return view('owner.articles.create');
    }

    public function show($id)
    {
        $article = NewspaperArticle::find($id);
        return view('owner.articles.show', ['article' => $article, 'activeMenu' => 'articles']);
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $newspaper = Newspaper::where('user_id', $user->id)->first();
        $article =  new NewspaperArticle();
        $article->title = $request->input('title');
        $article->content = $request->input('content');
        $article->approved = 1;
        //TODO UPLOAD PICTURE
        $article->user_id = $user->id;
        $article->newspaper_id = $newspaper->id;
        $article->save();
        return redirect()->route('owner.articles.index');
    }

    public function approve(Request $request)
    {
        $article = NewspaperArticle::find($request->input('id'));
        $article->approved = 1;
        $article->save();
        return redirect()->route('owner.articles.index');
    }

    public function edit($id)
    {
        $article = NewspaperArticle::find($id);
        return view('owner.articles.edit', ['article' => $article, 'activeMenu' => 'articles']);
    }

    public function update(Request $request, $id)
    {
        $article = NewspaperArticle::find($id);
        $article->title = $request->input('title');
        $article->content = $request->input('content');
        //TODO update picture
        $article->update();
        return redirect()->route('owner.articles.index');
    }

    public function destroy($id)
    {
        NewspaperArticle::where('id', $id)->delete();
        return redirect()->route('owner.articles.index');
    }


}
