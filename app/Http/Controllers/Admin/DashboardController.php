<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

use App\Http\Requests;

class DashboardController extends Controller
{
    public function index()
    {
        $users = User::paginate(10);
        return view('admin.dashboard', ['users' => $users, 'activeMenu' => 'dashboard']);
    }

    public function destroy($id)
    {
        User::where('id', $id)->delete();
        return redirect()->route('admin.dashboard');
    }
}
