<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Newspaper;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\NewspaperEmployee;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;

class NewspapersController extends Controller
{
    public function index()
    {
        $newspapers = Newspaper::join('users', 'newspapers.user_id', '=', 'users.id')
                 ->selectRaw('newspapers.*, users.name as user_name, users.surname as user_surname')
                 ->paginate(10);

        return view('admin.newspapers.index', ['newspapers' => $newspapers, 'activeMenu' => 'newspapers']);
    }

    public function create()
    {
        $users = User::where('hired', 0)->get();

        return view('admin.newspapers.create', ['users' => $users, 'activeMenu' => 'newspapers']);
    }

    public function store(Request $request)
    {
        $newspaper = new Newspaper();
        $newspaper->newspapers_name = $request->input('name');
        $newspaper->user_id = $request->input('owner');
        $file = $request->file('image');

        $extension = $file->getClientOriginalExtension();
        $filename = microtime()*10000 . $newspaper->newspapers_name . '.' . $extension;
        $file->move('images/newspapers', $filename);
        $newspaper->img_url = $filename;
        $newspaper->save();
        $user = User::find($request->input('owner'));
        $user->hired = 1;
        $user->update();
        return redirect()->route('admin.newspapers.index');
    }

    public function edit($id)
    {
        $newspaper = Newspaper::find($id);
        $users = NewspaperEmployee::join('users', 'users.id', '=', 'newspaper_employees.user_id')
                                  ->where('newspaper_employees.newspaper_id', $id)
                                  ->selectRaw('users.*')
                                  ->get();
        $owner = User::find($newspaper->user_id);

        return view('admin.newspapers.edit', ['newspaper' => $newspaper, 'users' => $users, 'owner' => $owner, 'activeMenu' => 'newspapers']);
    }

    public function update($id, Request $request)
    {

        $newspaper = Newspaper::find($id);
        //Old boss gets fired

        $user = User::find($newspaper->user_id);
        $user->hired = 0;
        $user->type = 2;
        $user->update();
        //Change role from owner to user
        DB::table('role_user')->where('user_id', $user->id)->update(['role_id' => 4]);
        DB::table('role_user')->where('user_id', $request->input('owner'))->update(['role_id' => 2]);
        DB::table('newspaper_employees')->where('user_id', $request->input('owner'))->delete();
        DB::table('users')->where('id', $request->input('owner'))->update(['type' => 1, 'hired' => 1]);

        if($request->hasFile('image')) {
            unlink('images/newspapers/' . $newspaper->img_url);
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = microtime()*10000 . $newspaper->newspapers_name . '.' . $extension;
            $file->move('images/newspapers', $filename);
            $newspaper->img_url = $filename;
        }
        $newspaper->newspapers_name = $request->input('name');
        $newspaper->user_id = $request->input('owner');
        $newspaper->save();
        return redirect()->route('admin.newspapers.index');
    }

    public function destroy($id)
    {
        Newspaper::where('id', $id)->delete();
        return redirect()->route('admin.newspapers.index');
    }
}
