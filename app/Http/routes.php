<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'UserController@index');


Route::auth();

Route::get('/home', 'HomeController@index')->name('homepage');

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('redirect', function()
{
    if(Entrust::hasRole('admin')) {
    return redirect()->route('admin.dashboard');
    } else if(Entrust::hasRole('owner') ) {
    return redirect()->route('owner.dashboard');
    } else if(Entrust::hasRole('employee') ) {
        return redirect()->route('employee.articles.index');
    }  else {
        return redirect()->route('homepage');
    }
});

Route::group(['namespace' => 'owner', 'prefix' => 'owner', 'middleware' => ['role:owner']], function () {
    Route::get('edit-newspaper-info', 'DashboardController@editInfo')->name('owner.edit-newspaper-info');
    Route::post('edit-info', 'DashboardController@editInfoPost')->name('owner.edit-info');
    Route::get('dashboard', 'DashboardController@index')->name('owner.dashboard');
    Route::get('hire', 'DashboardController@hire')->name('owner.hire');
    Route::post('owner.hire-new', 'DashboardController@hireNew')->name('owner.hire-new');
    Route::post('fire-user', 'DashboardController@fireUser')->name('owner.fire-user');
    Route::post('articles.approve', 'ArticlesController@approve')->name('owner.articles.approve');
    Route::resource('articles', 'ArticlesController');

});

Route::group(['namespace' => 'admin', 'prefix' => 'admin', 'middleware' => ['role:admin']], function () {
    Route::get('dashboard', [ 'uses' => 'DashboardController@index', 'as' => 'admin.dashboard']);
    Route::resource('users', 'DashboardController', ['except' => ['create', 'store', 'edit', 'update']]);
    Route::resource('newspapers', 'NewspapersController');
});

Route::group(['namespace' => 'employee', 'prefix' => 'employee', 'middleware' => ['role:employee']], function () {
    Route::resource('articles', 'ArticlesController');
});