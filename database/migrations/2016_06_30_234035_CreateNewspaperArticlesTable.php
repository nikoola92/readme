<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewspaperArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('newspaper_articles', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('newspaper_id')->unsigned();
            $table->integer('user_id');
            $table->boolean('approved');
            $table->text('content');
            $table->string('title');
            $table->string('img_url');
            //$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            //$table->foreign('newspaper_id')->references('id')->on('newspapers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('newspaper_articles');
    }
}
