<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('user_id')->unsigned();
            $table->integer('article_id')->unsigned();
            $table->string('comment');
            $table->integer('grade');
            $table->boolean('approved');
            //$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            // $table->foreign('article_id')->references('id')->on('newspaper_articles')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reviews');
    }
}
