<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('user1_id')->unsigned();
            $table->integer('user2_id')->unsigned();
            $table->integer('article_id');
            $table->integer('review_id');
            $table->string('content');
            $table->integer('type');
            //$table->foreign('user1_id')->references('id')->on('users')->onDelete('cascade');
            //$table->foreign('user2_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notifications');
    }
}
