<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangedArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('changed_articles', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('user1_id')->unsigned();
            $table->integer('user2_id')->unsigned();
            $table->integer('article_id');
            $table->string('img_url');
            $table->string('content');
            $table->boolean('approved');
            //$table->foreign('user1_id')->references('id')->on('users')->onDelete('cascade');
            //$table->foreign('article_id')->references('id')->on('newspaper_articles')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('changed_articles');
    }
}
