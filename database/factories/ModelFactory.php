<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->userName,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(64)),
        'type' => 3,
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'image_url' => $faker->imageUrl(),
        'remember_token' => str_random(10)
    ];
});


/*$factory->define(App\Models\ChangedArticle::class, function (Faker\Generator $faker) {
    return [
        'user1_id' => $faker->unique()->randomNumber,
        'user2_id' => $faker->unique()->randomNumber,
        'user2_id' => $faker->unique()->randomNumber,
        'article_id' => $faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'content' => str_random(100),
        'img_url' => str_random(15),
        'approved' => $faker->randomElement([0,1])
    ];
});*/

/*$factory->define(App\Models\Newspaper::class, function (Faker\Generator $faker) {
    return [
        'newspapers_name' => str_random(15),
        'img_url' => $faker->imageUrl()
    ];
});*/

//$factory->define(App\Models\NewspaperArticle::class, function (Faker\Generator $faker) {
//    return [
//        'username' => $faker->userName,
//        'email' => $faker->safeEmail,
//        'password' => bcrypt(str_random(64)),
//        'type' => $faker->randomElement([0,1,2,3]),
//        'name' => $faker->name,
//        'surname' => $faker->lastName,
//        'img_url' => $faker->imageUrl(),
//        'remember_token' => str_random(10)
//    ];
//});
//$factory->define(App\Models\NewspaperEmployee::class, function (Faker\Generator $faker) {
//    return [
//        'username' => $faker->userName,
//        'email' => $faker->safeEmail,
//        'password' => bcrypt(str_random(64)),
//        'type' => $faker->randomElement([0,1,2,3]),
//        'name' => $faker->name,
//        'surname' => $faker->lastName,
//        'img_url' => $faker->imageUrl(),
//        'remember_token' => str_random(10)
//    ];
//});
//$factory->define(App\Models\Notification::class, function (Faker\Generator $faker) {
//    return [
//        'username' => $faker->userName,
//        'email' => $faker->safeEmail,
//        'password' => bcrypt(str_random(64)),
//        'type' => $faker->randomElement([0,1,2,3]),
//        'name' => $faker->name,
//        'surname' => $faker->lastName,
//        'img_url' => $faker->imageUrl(),
//        'remember_token' => str_random(10)
//    ];
//});
//$factory->define(App\Models\Review::class, function (Faker\Generator $faker) {
//    return [
//        'username' => $faker->userName,
//        'email' => $faker->safeEmail,
//        'password' => bcrypt(str_random(64)),
//        'type' => $faker->randomElement([0,1,2,3]),
//        'name' => $faker->name,
//        'surname' => $faker->lastName,
//        'img_url' => $faker->imageUrl(),
//        'remember_token' => str_random(10)
//    ];
//});
