<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Models\Permission;
use App\Models\User;
use App\Models\Newspaper;
use App\Models\NewspaperEmployee;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        Role::create([
            'name' => 'admin',
            'display_name' => 'Admin',
            'description' => 'User is the Admin of the website',
        ]);
        Role::create([
            'name' => 'owner',
            'display_name' => 'Owner',
            'description' => 'User is the Owner of some newspaper',
        ]);
        Role::create([
            'name' => 'employee',
            'display_name' => 'Employee',
            'description' => 'User is the Employee of some newspaper',
        ]);
        Role::create([
            'name' => 'user',
            'display_name' => 'user',
            'description' => 'User is websites client',
        ]);

        $admin = new User();
        $admin->username = 'admin';
        $admin->email = 'admin@mail.com';
        $admin->password = bcrypt('123123');
        $admin->type = 0;
        $admin->name = 'ad';
        $admin->surname = 'min';
        $admin->hired = 1;
        $role = Role::where('name', 'admin')->first();
        $admin->save();
        $admin->attachRole($role);
        $admin->update();

        $owner = new User();
        $owner->username = 'owner';
        $owner->email = 'owner@mail.com';
        $owner->password = bcrypt('123123');
        $owner->type = 1;
        $owner->name = 'ow';
        $owner->surname = 'ner';
        $owner->hired = 1;
        $role = Role::where('name', 'owner')->first();
        $owner->save();
        $owner->attachRole($role);
        $owner->update();

        $newspaper = new Newspaper();
        $newspaper->user_id = $owner->id;
        $newspaper->newspapers_name = "TIME";
        $newspaper->save();

        $employee = new User();
        $employee->username = 'employee';
        $employee->email = 'employee@mail.com';
        $employee->password = bcrypt('123123');
        $employee->type = 2;
        $employee->name = 'em';
        $employee->surname = 'ployee';
        $employee->hired = 1;
        $role = Role::where('name', 'employee')->first();
        $employee->save();
        $employee->attachRole($role);
        $employee->update();
        $nespaper_employee = new NewspaperEmployee();
        $nespaper_employee->user_id = $employee->id;
        $nespaper_employee->newspaper_id = $newspaper->id;
        $nespaper_employee->save();

        $user = new User();
        $user->username = 'user';
        $user->email = 'user@mail.com';
        $user->password = bcrypt('123123');
        $user->type = 4;
        $user->name = 'user';
        $user->surname = 'default';
        $user->hired = 0;
        $user->save();

        $this->call(UsersTableSeeder::class);

    }
}
