<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 25)->create()->each(function(User $user) {
            DB::table('role_user')->insert([
                      'user_id' => $user->id,
                      'role_id' => 4
                ]);
        });;
    }
}
