<?php

use Illuminate\Database\Seeder;
use App\Models\Newspaper;

class NewspapersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Newspaper::class, 5)->create();
    }
}
