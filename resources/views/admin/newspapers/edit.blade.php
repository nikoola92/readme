@extends('admin.layout')

@section('title')
    Edit {{ $newspaper->newspapers_name }} Newspaper
@endsection

@section('head')
    <link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <!-- BEGIN FORM-->
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form method="post" action="{{ route('admin.newspapers.update', $newspaper->id) }}" class="account-form" enctype="multipart/form-data">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control input-circle" placeholder="Name" name="name" id="name" value="{{ old('name', $newspaper->newspapers_name) }}">
                            </div>
                            <div class="form-group">
                                <label for="owner">Owner</label>
                                <select class="selectpicker form-control" name="owner" id="type">
                                    <option value="">Please choose...</option>
                                    <option value="{{  $owner->id }}" @if (old('owner_id', $newspaper->user_id) ==  $owner->id) selected @endif> {{  $owner->username }}</option>
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}" @if (old('owner_id', $newspaper->user_id) == $user->id) selected @endif> {{ $user->username }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Image</label> <br>
                            <div data-provides="fileinput" class="fileinput fileinput-new">
                                <div>
                                    <img src="{{ asset('images/newspapers/'.$newspaper->img_url) }}" style="width: 300px; height: 100px;" />
                                </div>
                                <div class="input-group input-large">
                                    <div data-trigger="fileinput" class="form-control uneditable-input input-fixed input-medium">
                                        <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                        <span class="fileinput-filename"> </span>
                                    </div>
                                        <span class="input-group-addon btn default btn-file">
                                            <span class="fileinput-new">  Select image  </span>
                                            <span class="fileinput-exists"> Change </span>
                                            <input type="file" name="image">
                                        </span>
                                    <a data-dismiss="fileinput" class="input-group-addon btn purple fileinput-exists" href="javascript:;"> Remove </a>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button class="btn blue" type="submit">Edit</button>
                            <a class="btn blue btn-outline sbold upppercase" href="{{ route('admin.newspapers.index') }}"> Cancel </a>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT">
                    </form>
                </div>
            </div>
            <!-- END FORM-->
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection