@extends('admin.layout')

@section('title')
    Create new Newspaper
@endsection

@section('head')
    <link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/general.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <!-- BEGIN FORM-->
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form method="post" action="{{ route('admin.newspapers.store') }}" class="newspapers-create-form" enctype="multipart/form-data">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control input-circle" placeholder="Name" name="name" id="name" value="{{ old('name') }}">
                            </div>
                            <div class="form-group">
                                <label for="owner">Owner</label>
                                <select class="selectpicker form-control" name="owner" id="type">
                                    <option value="">Please choose...</option>
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}" @if (old('owner') == $user->id) selected @endif> {{ $user->username }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Image</label> <br>
                                <div data-provides="fileinput" class="fileinput fileinput-new">
                                    <div class="input-group input-large">
                                        <div data-trigger="fileinput" class="form-control uneditable-input input-fixed input-medium">
                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                            <span class="fileinput-filename"> </span>
                                        </div>

                                        <span class="input-group-addon btn default btn-file">
                                            <span class="fileinput-new">  Select image  </span>
                                            <span class="fileinput-exists"> Change </span>
                                            <input type="file" name="image"> <br>
                                        </span>
                                        <a data-dismiss="fileinput" class="input-group-addon btn purple fileinput-exists" href="javascript:;"> Remove </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button class="btn blue" type="submit">Create</button>
                            <a class="btn blue btn-outline sbold upppercase" href="{{ route('admin.newspapers.index') }}"> Cancel </a>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                </div>
            </div>
            <!-- END FORM-->
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/newspapers.js') }}" type="text/javascript"></script>
@endsection