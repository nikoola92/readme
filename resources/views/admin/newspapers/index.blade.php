@extends('admin.layout')
@section('title')
    Newspapers
@endsection
@section('content')
    <div class="row">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="actions">
                    <div class="btn-group">
                        <a class="btn blue  sbold upppercase" href="{{ route('admin.newspapers.create') }}"> Add New Newspaper </a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">

                <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th scope="col"> Image </th>
                            <th scope="col"> Name </th>
                            <th scope="col"> Owner </th>
                            <th scope="col"> Actions </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($newspapers as $newspaper)
                            <tr>
                                <td> <img src="{{ asset('images/newspapers/' . $newspaper->img_url) }}" style="height: 100px;"/> </td>
                                <td> {{ $newspaper->newspapers_name }} </td>
                                <td> {{ $newspaper->user_name }} &nbsp; {{ $newspaper->user_surname }} </td>
                                <td>
                                    <div class="actions">
                                        <a data-trigger="hover" href="{{ route('admin.newspapers.edit', [$newspaper->id]) }}" class="btn btn-circle btn-icon-only btn-default tooltips " data-original-title="Edit Newspaper">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                        <a data-trigger="hover" href="#deleteModal" class="btn btn-circle btn-icon-only btn-default tooltips icon-delete" data-original-title="Delete Newspaper" data-toggle="modal">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                        <form method="post" action="{{ route('admin.newspapers.destroy', [$newspaper->id]) }}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $newspapers->render() }}
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
    @include('admin.index-delete-modal', ['notion' => 'Newspaper'])
@endsection

@section('scripts')
    <script src="{{ asset('js/general.js') }}" type="text/javascript"></script>
@endsection