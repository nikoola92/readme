@extends('admin.layout')
@section('title')
    All Users
@endsection
@section('content')
    <div class="row">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="actions">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
            <div class="portlet-body">

                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th scope="col"> Username </th>
                                <th scope="col"> E-mail </th>
                                <th scope="col"> Role </th>
                                <th scope="col"> Actions </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td> {{ $user->username }} </td>
                                    <td> {{ $user->email }} </td>
                                    <td> {{ $user->getUserType($user->type) }} </td>
                                    <td>
                                        <div class="actions">
                                            <a data-trigger="hover" href="#deleteModal" class="btn btn-circle btn-icon-only btn-default tooltips icon-delete" data-original-title="Delete User" data-toggle="modal">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                            <form method="post" action="{{ route('admin.users.destroy', [$user->id]) }}">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="_method" value="DELETE">
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                {{ $users->render() }}
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
    @include('admin.index-delete-modal', ['notion' => 'User'])
@endsection

@section('scripts')
    <script src="{{ asset('js/general.js') }}" type="text/javascript"></script>
@endsection