@extends('owner.layout')
@section('title')
    Hire new employee
@endsection
@section('content')
    <div class="row">
        <div class="col-md-6">
            <!-- BEGIN FORM-->
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form method="post" action="{{ route('owner.hire-new') }}" class="account-form">
                        <div class="form-body">
                            <select class="selectpicker form-control" name="user_id" id="type">
                                <option value="">Please choose...</option>
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}" @if (old('user_id') == $user->id) selected @endif> {{ $user->username }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-actions">
                            <button class="btn blue" type="submit">Hire</button>
                            <a class="btn blue btn-outline sbold upppercase" href="{{ route('owner.articles.index') }}"> Cancel </a>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                </div>
            </div>
            <!-- END FORM-->
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('admin-assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
@endsection