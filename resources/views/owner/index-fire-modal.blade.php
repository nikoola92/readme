<div id="deleteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Warning</h4>
            </div>
            <div class="modal-body">
                <p> Are you sure you want to fire {{ $notion }}? </p>
            </div>
            <div class="modal-footer">
                <a class="btn blue btn-outline sbold upppercase modal-cancel-delete-btn" href="#" data-dismiss="modal"> Cancel </a>
                <button class="btn blue modal-delete-btn" type="submit">Delete</button>
            </div>
        </div>
    </div>
</div>