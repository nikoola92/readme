@extends('owner.layout')
@section('title')
    Edit {{ $article->title }} Article
@endsection
@section('content')
    <div class="row">
        <div class="col-md-6">
            <!-- BEGIN FORM-->
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form method="post" action="{{ route('owner.articles.update', [ $article->id ]) }}" class="account-form">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control input-circle" placeholder="Title" name="title" id="title" value="{{ old('title', $article->title) }}">
                            </div>
                            <div class="form-group">
                                <label for="content">Content</label>
                                <textarea rows="10" class="form-control" name="content" id="content">{{ old('content', $article->content) }}</textarea>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button class="btn blue" type="submit">Edit</button>
                            <a class="btn blue btn-outline sbold upppercase" href="{{ route('owner.articles.index') }}"> Cancel </a>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT">
                    </form>
                </div>
            </div>
            <!-- END FORM-->
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('admin-assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
@endsection