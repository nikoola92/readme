@extends('owner.layout')
@section('title')
    Articles
@endsection
@section('content')
    <div class="row">
        <div class="portlet box blue">
            <div class="portlet-title">
                <h3>Approved Articles</h3>
                <div class="actions">
                    <div class="btn-group">
                        <a class="btn blue  sbold upppercase" href="{{ route('owner.articles.create') }}"> Add New Article </a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">

                <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th scope="col"> Title </th>
                            <th scope="col"> Author </th>
                            <th scope="col"> Actions </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($articles as $article)
                            <tr>
                                <td> <a href="{{ route('owner.articles.show', [$article->id]) }}">{{ $article->title }}</a> </td>
                                <td> {{ $article->username }} </td>
                                <td>
                                    <div class="actions">
                                        <a data-trigger="hover" href="{{ route('owner.articles.edit', [$article->id]) }}" class="btn btn-circle btn-icon-only btn-default tooltips " data-original-title="Edit Article">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                        <a data-trigger="hover" href="#deleteModal" class="btn btn-circle btn-icon-only btn-default tooltips icon-delete" data-original-title="Delete Article" data-toggle="modal">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                        <form method="post" action="{{ route('owner.articles.destroy', [$article->id]) }}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $articles->render() }}
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
        <div class="portlet box blue">
            <div class="portlet-title">
                <h3>Pending Articles</h3>
            </div>
            <div class="portlet-body">

                <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th scope="col"> Title </th>
                            <th scope="col"> Author </th>
                            <th scope="col"> Actions </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($pendingArticles as $pendingArticle)
                            <tr>
                                <td> <a href="{{ route('owner.articles.show', [$pendingArticle->id]) }}">{{ $pendingArticle->title }}</a> </td>
                                <td> {{ $pendingArticle->username }} </td>
                                <td>
                                    <div class="actions">
                                        <a data-trigger="hover" href="{{ route('owner.articles.edit', [$pendingArticle->id]) }}" class="btn btn-circle btn-icon-only btn-default tooltips " data-original-title="Edit Article">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                        <form method="post" action="{{ route('owner.articles.approve') }}">
                                            <a data-trigger="hover" href="#" class="btn btn-circle btn-icon-only btn-default tooltips article-btns" data-original-title="Approve Article">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="id" value="{{ $pendingArticle->id }}">
                                        </form><form method="post" action="{{ route('owner.articles.destroy', [$pendingArticle->id]) }}">
                                            <a data-trigger="hover" href="#" class="btn btn-circle btn-icon-only btn-default tooltips article-btns" data-original-title="Disapprove Article">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
    @include('admin.index-delete-modal', ['notion' => 'Article'])
@endsection

@section('scripts')
    <script src="{{ asset('js/general.js') }}" type="text/javascript"></script>
@endsection