@extends('owner.layout')

@section('title')
    Dashboard
@endsection

@section('content')
    <div class="row">
        <div class="portlet box blue">
            <div class="portlet-title">
                <h3>Newspaper Info</h3>
                <div class="actions">
                    <div class="btn-group">
                        <a class="btn blue  sbold upppercase" href="{{ route('owner.edit-newspaper-info') }}"> Edit info </a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Name</label>
                            <label class="form-control input-circle"> {{ $newspaper->newspapers_name }} </label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
        <div class="portlet box blue">
            <div class="portlet-title">
                <h3>Employees</h3>
                <div class="actions">
                    <div class="btn-group">
                        <a class="btn blue  sbold upppercase" href="{{ route('owner.hire') }}"> Hire New Employee </a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">

                <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th scope="col"> Username </th>
                            <th scope="col"> E-mail </th>
                            <th scope="col"> Actions </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td> {{ $user->username }} </td>
                                <td> {{ $user->email }} </td>
                                <td>
                                    <div class="actions">
                                        <a data-trigger="hover" href="#deleteModal" class="btn btn-circle btn-icon-only btn-default tooltips icon-delete" data-original-title="Fire User" data-toggle="modal">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                        <form method="post" action="{{ route('owner.fire-user') }}">
                                            <input type="hidden" name="id" value="{{ $user->id }}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
    @include('owner.index-fire-modal', ['notion' => 'User'])
@endsection

@section('scripts')
    <script src="{{ asset('js/general.js') }}" type="text/javascript"></script>
@endsection