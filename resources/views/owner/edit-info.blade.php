@extends('owner.layout')
@section('title')
    Edit {{ $newspaper->newspapers_name }} Info
@endsection
@section('content')
    <div class="row">
        <div class="col-md-6">
            <!-- BEGIN FORM-->
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form method="post" action="{{ route('owner.edit-info') }}" class="account-form">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="title">Name</label>
                                <input type="text" class="form-control input-circle" placeholder="Name" name="name" id="name" value="{{ old('name', $newspaper->newspapers_name) }}">
                            </div>
                        </div>
                        <div class="form-actions">
                            <button class="btn blue" type="submit">Edit</button>
                            <a class="btn blue btn-outline sbold upppercase" href="{{ route('owner.dashboard') }}"> Cancel </a>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $newspaper->id }}">
                    </form>
                </div>
            </div>
            <!-- END FORM-->
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('admin-assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
@endsection