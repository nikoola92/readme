@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Newest articles</div>

                <div class="panel-body">
                    You are logged in!
                    @foreach($articles as $article)
                        <div class="row">
                            {{ $article->name }}
                        </div>
                    @endforeach
                    {{ $articles->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
