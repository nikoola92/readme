@extends('employee.layout')
@section('title')
    Articles
@endsection
@section('content')
    <div class="row">
        <div class="portlet box blue">
            <div class="portlet-title">
                <h3>Approved Articles</h3>
                <div class="actions">
                    <div class="btn-group">
                        <a class="btn blue  sbold upppercase" href="{{ route('employee.articles.create') }}"> Add New Article </a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">

                <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th scope="col"> Title </th>
                            <th scope="col"> Actions </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($articles as $article)
                            <tr>
                                <td> <a href="{{ route('employee.articles.show', [$article->id]) }}">{{ $article->title }}</a> </td>
                                <td>
                                    <div class="actions">
                                        <a data-trigger="hover" href="{{ route('employee.articles.edit', [$article->id]) }}" class="btn btn-circle btn-icon-only btn-default tooltips " data-original-title="Edit Article">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $articles->render() }}
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/general.js') }}" type="text/javascript"></script>
@endsection