@extends('employee.layout')

@section('content')
<div class="row">
    <div class="col-md-6">
        <!-- BEGIN FORM-->
        <div class="portlet light bordered">
            <div class="portlet-body form">
                <form role="form">
                    <div class="form-body">
                        <div class="form-group">
                            <label>Title</label>
                            <label class="form-control input-circle"> {{ $article->title }} </label>
                        </div>
                        <div class="form-group">
                            <label>Content</label>
                            <textarea rows="10" class="form-control readonly-element" readonly>{{ $article->content }}</textarea>
                        </div>
                    </div>
                    <div class="form-actions">
                        <a class="btn blue btn-outline sbold upppercase" href="{{ route('employee.articles.edit', [$article->id]) }}"> Edit </a>
                        <a class="btn blue btn-outline sbold upppercase " href="{{ route('employee.articles.index') }}" data-toggle="modal"> Back </a>
                    </div>
                </form>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>
@endsection