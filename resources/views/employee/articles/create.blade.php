@extends('employee.layout')
@section('title')
    Create new Article
@endsection

@section('head')
    <link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <!-- BEGIN FORM-->
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form method="post" action="{{ route('employee.articles.store') }}" class="account-form" enctype="multipart/form-data">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control input-circle" placeholder="Name" name="title" id="title" value="{{ old('title') }}">
                            </div>
                            <div class="form-group">
                                <label for="content">Content</label>
                                <textarea rows="10" class="form-control" name="content" id="content">{{ old('content') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Image</label> <br>
                                <div data-provides="fileinput" class="fileinput fileinput-new">
                                    <div class="input-group input-large">
                                        <div data-trigger="fileinput" class="form-control uneditable-input input-fixed input-medium">
                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                            <span class="fileinput-filename"> </span>
                                        </div>
                                        <span class="input-group-addon btn default btn-file">
                                            <span class="fileinput-new">  Select image  </span>
                                            <span class="fileinput-exists"> Change </span>
                                            <input type="file" name="image">
                                        </span>
                                        <a data-dismiss="fileinput" class="input-group-addon btn purple fileinput-exists" href="javascript:;"> Remove </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button class="btn blue" type="submit">Create</button>
                            <a class="btn blue btn-outline sbold upppercase" href="{{ route('employee.articles.index') }}"> Cancel </a>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                </div>
            </div>
            <!-- END FORM-->
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

