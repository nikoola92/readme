<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>ReadMe - Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="Neopix" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{ asset('assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{ asset('assets/pages/css/login.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <!-- BEGIN CUSTOM LAYOUT STYLES -->
    <link href="{{ asset('styles/admin/style.css') }}" rel="stylesheet" type="text/css" />
    <!-- END CUSTOM LAYOUT STYLES -->
    <link rel="shortcut icon" href="{{ asset('uu-favicon.ico') }}" /> </head>
<!-- END HEAD -->

<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="{{ route('homepage') }}"><img src="{{ asset('images/admin/logo.png') }}" alt="ReadMe" /></a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" action="{{ url('/register') }}" method="post" id="loginForm">
        {{ csrf_field() }}
        <h3 class="form-title font-blue">Register</h3>
        <div class="alert alert-danger @if (!empty($errors)) display-hide @endif">
            <button class="close" data-close="alert"></button>
            <span> There are some errors. </span>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9" for="email">E-mail</label>
            <input class="form-control form-control-solid placeholder-no-fix" id="email" type="email" autocomplete="off" placeholder="E-mail" name="email" value="{{ old("email") }}"/>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9" for="password">Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" id="password" type="password" autocomplete="off" placeholder="Password" name="password" value="{{ old("password") }}"/>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9" for="password">Name</label>
            <input class="form-control form-control-solid placeholder-no-fix" id="password" type="text" autocomplete="off" placeholder="Name" name="name" value="{{ old("name") }}"/>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9" for="password">Surname</label>
            <input class="form-control form-control-solid placeholder-no-fix" id="password" type="text" autocomplete="off" placeholder="Surname" name="surname" value="{{ old("surname") }}" />
        </div>

        <div class="form-actions">
            <button type="submit" class="btn blue uppercase">Register</button>
            <label class="rememberme check"><input type="checkbox" name="remember" value="1" />Remember</label>
        </div>
    </form>
    <div style="width: 100%;"><center><a class="btn blue btn-outline sbold upppercase" href="{{ route('homepage') }}"> Cancel </a></center></div>
</div>
<div class="copyright"> 2016 © ReadMe. Sva prava zadržana. </div>
<!--[if lt IE 9]>
<script src="{{ asset('assets/global/plugins/respond.min.js') }}"></script>
<script src="{{ asset('assets/global/plugins/excanvas.min.js') }}"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('scripts/admin/bundle.min.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>